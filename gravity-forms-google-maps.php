<?php
/**
 *
 * @package   Gravity_Forms_Google_Maps
 * @author    Daniel Abernathy <dabernathy89@gmail.com>
 * @license   GPL-2.0+
 * @copyright 2013 Daniel Abernathy
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Forms Google Maps Add-on
 * Description:       Turn Gravity Forms entries into custom google maps
 * Version:           2.0.0
 * Author:            Daniel Abernathy
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action('plugins_loaded', 'gravity_forms_google_maps_setup_class', 10);

function gravity_forms_google_maps_setup_class() {
	if (class_exists("GFForms")) {
		GFForms::include_addon_framework();
	    GFForms::include_feed_addon_framework();
        require_once( plugin_dir_path( __FILE__ ) . '/includes/class-google-maps-form-settings.php' );
		require_once( plugin_dir_path( __FILE__ ) . '/includes/class-google-maps-feeds.php' );
        require_once( plugin_dir_path( __FILE__ ) . '/includes/class-google-maps-shortcode.php' );
        require_once( plugin_dir_path( __FILE__ ) . '/includes/class-google-maps-ajax.php' );
        $form_map_settings = Gravity_Forms_Google_Maps_Form_Settings::get_instance();
		$map_feeds = Gravity_Forms_Google_Maps_Feeds::get_instance();
        Gravity_Forms_Google_Maps_Shortcode::get_instance( $form_map_settings );
        Gravity_Forms_Google_Maps_Ajax::get_instance( $form_map_settings, $map_feeds );
	}
}
