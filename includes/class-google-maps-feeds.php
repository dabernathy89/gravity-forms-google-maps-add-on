<?php
/**
 * Gravity Forms Google Maps
 *
 * @package   Gravity_Forms_Google_Maps
 * @author    Daniel Abernathy <dabernathy89@gmail.com>
 * @license   GPL-2.0+
 * @copyright 2013 Daniel Abernathy
 */

class Gravity_Forms_Google_Maps_Feeds extends GFFeedAddOn {

	protected static $instance = null;
    protected $_version = "2.0.0";
    protected $_min_gravityforms_version = "1.8.5";
    protected $_slug = "gravity-forms-google-maps-feeds";
    protected $_path;
    protected $_full_path = __FILE__;
    protected $_title = "Gravity Forms Google Maps Add-On Feeds";
    protected $_short_title = "Google Maps Feeds";

    public function __construct() {
        $this->_path = plugin_basename( __FILE__ );
        parent::__construct();
    }

    public function init(){
        parent::init();
    }

    public function pre_init() {
        parent::pre_init();

        // Enqueue Google Maps and our custom javascript
        add_action('gform_enqueue_scripts', array($this,"enqueue_google_map_scripts"), 10, 2);

        // add a class to the form
        add_filter('gform_pre_render', array($this, 'add_form_class'));
    }

    public function scripts() {
        $scripts = array(
            array(
                  "handle"  => "gf_google_map_feed_js",
                  "src"     => plugins_url( "../public/js/feed.js" , __FILE__ ),
                  "version" => $this->_version,
                  "deps"    => array("jquery"),
                  "enqueue" => array(
                      array(
                          "admin_page" => array("form_settings"),
                          "tab"        => "gravity-forms-google-maps-feeds"
                      )
                  )
            ),
        );
        return array_merge(parent::scripts(), $scripts);
    }

    public function enqueue_google_map_scripts($form="", $is_ajax=false) {
        if (!empty($form)) {
            $feeds = $this->get_feeds($form['id']);
            if ( $feeds ) {
                $api_key = get_option( 'gf_gm_google_api_key', '' );
                wp_enqueue_script( 'googlemaps', 'https://maps.googleapis.com/maps/api/js?key=' . urlencode($api_key), array(), null, true );
                wp_enqueue_script( 'gravity_forms_google_maps_js', plugins_url( "../public/js/public.js" , __FILE__ ), array('jquery'), $this->_version, true );
            }
        }
    }

    public function feed_settings_fields() {

        $zoom_level_choices = array();
        for ($i=0; $i < 20; $i++) {
            $zoom_level_choices[$i] = array(
                "label" => (string)$i,
                "name"  => "zoom_level_" . $i
            );
        }

        return array(
            array(
                "title"  => "Google Maps Settings",
                "fields" => array(
                    array(
                        "label" => "Feed Name",
                        "type" => "text",
                        "name" => "feed_name"
                    ),
                    array(
                        "label"   => "Icon (image url)",
                        "type"    => "text",
                        "name"    => "map_pin"
                    ),
                    array(
                        "label" => "Google Maps InfoWindow Text",
                        "type" => "textarea",
                        "name" => "infowindow_text",
                        "tooltip" => "Use the dropdown by the textarea to insert data from the form entry."
                    ),
                    array(
                        "label"   => "Prevent icon overlap",
                        "type"    => "checkbox",
                        "name"    => "prevent_overlap",
                        "choices" => array(
                            array(
                                "label" => "Prevent icon overlap by shifting icons a short, random distance",
                                "name"  => "prevent_overlap_checkbox"
                            )
                        )
                    ),
                    array(
                        "name" => "condition",
                        "label" => "Condition",
                        "type" => "feed_condition",
                        "checkbox_label" => "Make this feed conditional",
                        "instructions" => "Process this feed if"
                    ),
                )
            ),
            array(
                "title" => "Rebuild Pins",
                "fields" => array(
                    array(
                        "label" => "Geocode empty entries",
                        "type" => "geocode_empty_pins_button",
                        "name" => "geocode_empty_pins",
                        "tooltip" => "Recalculate the latitude and longitude for pins that have not been geolocated."
                    ),
                    array(
                        "label" => "Rebuild pins for this feed",
                        "type" => "rebuild_pins_button",
                        "name" => "rebuild_pins",
                        "tooltip" => "Be careful if you are changing conditions. This will re-evaluate all form entries."
                    )
                )
            )
        );
    }

    public function settings_geocode_empty_pins_button() {
        echo "<button class='button geocode-empty-pins' data-current-feed='" . htmlentities($_GET['fid']) . "' data-current-form='" . htmlentities($_GET['id']) ."'>Geocode Entries</button>";
    }

    public function settings_rebuild_pins_button() {
        echo "<button class='button rebuild-pins' data-current-feed='" . htmlentities($_GET['fid']) . "' data-current-form='" . htmlentities($_GET['id']) ."'>Rebuild Pins</button>";
    }

    protected function populate_info_window_content($template, $entry, $form) {
        $template = GFCommon::replace_variables($template, $form, $entry, false, true, true);
        foreach ($entry as $key => $value) {
            if (is_numeric($key)) {
                $template = str_replace('{' . $key . '}', htmlentities($value), $template);
            }
        }
        return $template;
    }

    public function reprocess_feed( $feed, $entry, $form ) {
        // insert the entry values into the infowindow template
        $infowindow_template = $feed['meta']['infowindow_text'];
        $infowindow_content = $this->populate_info_window_content($infowindow_template, $entry, $form);

        $pin_image = $feed['meta']['map_pin'];

        if (!empty($infowindow_content)) {
            $infowindow_content = '<div style="line-height:1.35; overflow:hidden; white-space:nowrap;">' . $infowindow_content . "</div>";
        }

        gform_update_meta( $entry['id'], 'gravity_forms_google_maps_infowindow', $infowindow_content );
        gform_update_meta( $entry['id'], 'gravity_forms_google_maps_pin', $pin_image );

        // Set the lat/lng meta to blank value if it was not set at all previously
        $latlng = gform_get_meta( $entry['id'], 'gravity_forms_google_maps_coordinates' );

        if ($latlng === false) {
            gform_update_meta( $entry['id'], 'gravity_forms_google_maps_coordinates', '' );
        }
    }

    public function process_feed( $feed, $entry, $form ) {
        $latitude = $_POST['gravity_forms_google_maps_' . $form['id'] . '_lat'];
        $longitude = $_POST['gravity_forms_google_maps_' . $form['id'] . '_lng'];

        // insert the entry values into the infowindow template
        $infowindow_template = $feed['meta']['infowindow_text'];
        $infowindow_content = $this->populate_info_window_content($infowindow_template, $entry, $form);

        $pin_image = $feed['meta']['map_pin'];

        if (!empty($infowindow_content)) {
            $infowindow_content = '<div style="line-height:1.35; overflow:hidden; white-space:nowrap;">' . $infowindow_content . "</div>";
        }

        gform_update_meta( $entry['id'], 'gravity_forms_google_maps_infowindow', $infowindow_content );
        gform_update_meta( $entry['id'], 'gravity_forms_google_maps_pin', $pin_image );

        if (!$latitude || !$longitude) {
            gform_update_meta( $entry['id'], 'gravity_forms_google_maps_coordinates', '' );
        } else {

            // shift lat/lng slightly if we need to prevent overlap
            if ($feed['meta']['prevent_overlap_checkbox'] === "1") {
                // add random value between -0.009 and 0.009
                $latitude = (float)$latitude + (( rand(1,19) - 10 ) / 1000);
                $longitude = (float)$longitude + (( rand(1,19) - 10 ) / 1000);
            }

            gform_update_meta( $entry['id'], 'gravity_forms_google_maps_coordinates', $latitude . "," . $longitude );
        }

    }

    public function add_form_class($form) {
        $feeds = $this->get_feeds($form['id']);

        if ( $feeds ) {
            if (array_key_exists('cssClass', $form) && !empty($form['cssClass'])) {
                $form['cssClass'] = $form['cssClass'] . " gfgooglemap";
            } else {
                $form['cssClass'] = "gfgooglemap";
            }
        }

        return $form;
    }

    public function feed_list_columns() {
        return array(
            'feed_name' => 'Feed Name',
        );
    }

    public function feed_list_title(){
        $url = add_query_arg(array("fid" => "0"));
        return $this->get_short_title() . " <a class='add-new-h2' href='{$url}'>" . __("Add New", "gravityforms") . "</a>";
    }

	/**
	 * Return the plugin slug.
	 *
	 * @since    0.1.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.1.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}
