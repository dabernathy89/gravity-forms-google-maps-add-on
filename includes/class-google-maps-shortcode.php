<?php
/**
 * Gravity Forms Google Maps
 *
 * @package   Gravity_Forms_Google_Maps
 * @author    Daniel Abernathy <dabernathy89@gmail.com>
 * @license   GPL-2.0+
 * @copyright 2013 Daniel Abernathy
 */

class Gravity_Forms_Google_Maps_Shortcode {

    protected static $instance = null;
    protected static $_version = "2.0.0";
    protected $_min_gravityforms_version = "1.8.5";
    protected $_slug = "gravity-forms-google-maps-shortcode";
    protected $_path;
    protected $_full_path = __FILE__;
    protected $_title = "Gravity Forms Google Maps Add-On Shortcode";
    protected $_short_title = "Google Maps Shortcode";
    protected static $form_map_settings;

    public function __construct(Gravity_Forms_Google_Maps_Form_Settings $form_map_settings) {
        $this->_path = plugin_basename( __FILE__ );
        self::$form_map_settings = $form_map_settings;
        add_shortcode( 'gfgooglemap', array( $this, 'gfgooglemap_func' ) );
    }

    public static function gfgooglemap_func( $atts ) {
        $mapstring = "";
        $api_key = get_option( 'gf_gm_google_api_key', '' );

        // Return nothing if the formid isn't set on the shortcode
        if ( !$atts['formid'] ) {
            return '';
        }

        // Get shortcode arguments and set defaults
        extract( shortcode_atts( array(
            'width' => '100%',
            'height' => '400px',
            'formid' => false
        ), $atts ) );

        $form = GFAPI::get_form($formid);

        if ( !$form) {
            return '';
        }

        $map_settings = self::$form_map_settings->get_settings( $form );

        // Latitude and Longitude
        if (array_key_exists('map_center', $map_settings)) {
            $map_center = str_replace(' ', '', $map_settings['map_center']);
            $map_center = explode(',', $map_center);
            $lat = array_key_exists(0, $map_center) ? floatval($map_center[0]) : null;
            $lng = array_key_exists(1, $map_center) ? floatval($map_center[1]) : null;
        }

        if (empty($lat)) {
            $lat = 39.8282;
        }

        if (empty($lng)) {
            $lng = -98.5795;
        }

        // Map zoom level
        $map_zoom = (int)$map_settings['map_zoom_level'];
        if (empty($map_zoom)) {
            $map_zoom = 8;
        }

        // Map styles
        $map_settings['additional_map_options'] = trim($map_settings['additional_map_options'],"'");
        $map_settings['additional_map_options'] = ltrim($map_settings['additional_map_options'],"'");

        $jsdata = array(
            "zoom" => $map_zoom,
            "lat" => $lat,
            "lng" => $lng,
            "form_id" => $formid,
            "ajaxurl" => rest_url('gfgm/get-pins/' . $formid),
            "additional_map_options" => json_decode($map_settings['additional_map_options'])
        );

        // We're localizing this data to be accessed by our main JavaScript file
        wp_enqueue_script( 'googlemaps', 'https://maps.googleapis.com/maps/api/js?key=' . urlencode($api_key), array(), null, true );
        wp_enqueue_script( 'gravity_forms_google_maps_js', plugins_url( "../public/js/public.js" , __FILE__ ) , array('jquery'), self::$_version, true );
        wp_localize_script( 'gravity_forms_google_maps_js', 'gravity_forms_google_maps_settings_' . $formid, $jsdata );

        // Build the HTML for the map
        $mapstring .= "<style>#gravity-form-google-map-{$formid} img {max-width: none;}</style>\n" .
        "<div class='gravity-form-google-map' id='gravity-form-google-map-{$formid}' style='width: {$width}; height:{$height};'></div>\n";
        return $mapstring;
    }


    /**
     * Return the plugin slug.
     *
     * @since    0.1.0
     *
     * @return    Plugin slug variable.
     */
    public function get_plugin_slug() {
        return $this->_slug;
    }

    /**
     * Return an instance of this class.
     *
     * @since     0.1.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance( $form_map_settings ) {

        if ( null == self::$instance ) {
            self::$instance = new self( $form_map_settings );
        }

        return self::$instance;
    }

}
