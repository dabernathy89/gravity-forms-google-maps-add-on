<?php
/**
 * Gravity Forms Google Maps
 *
 * @package   Gravity_Forms_Google_Maps
 * @author    Daniel Abernathy <dabernathy89@gmail.com>
 * @license   GPL-2.0+
 * @copyright 2013 Daniel Abernathy
 */

class Gravity_Forms_Google_Maps_Ajax extends WP_REST_Controller {

    protected static $instance = null;
    protected $form_map_settings;
    protected $map_feeds;

    public function __construct( Gravity_Forms_Google_Maps_Form_Settings $form_map_settings, Gravity_Forms_Google_Maps_Feeds $map_feeds ) {

        $this->form_map_settings = $form_map_settings;
        $this->map_feeds = $map_feeds;

        add_action( 'rest_api_init', array( $this, 'register_routes' ) );

        // AJAX to rebuild pins
        add_action('wp_ajax_gravity_forms_google_map_geocode_empty_pins', array( $this, 'ajax_geocode_empty_pins' ) );
        add_action('wp_ajax_gravity_forms_google_map_rebuild_pins', array( $this, 'ajax_rebuild_pins' ) );
    }

    public function register_routes() {
        register_rest_route( 'gfgm', '/get-pins/(?P<formid>\d+)', array(
            'methods' => 'GET',
            'callback' => array( $this, 'ajax_get_pins' ),
            'args' => array(
                'formid' => array(
                    'validate_callback' => function($param, $request, $key) {
                        return is_numeric( $param );
                    }
                ),
            ),
        ) );
    }

    public function ajax_geocode_empty_pins() {
        $form_id = $_POST['form_id'];
        $feed_id = $_POST['feed_id'];
        $offset = (int)$_POST['offset'];
        $addresses = array();

        $form = GFAPI::get_form($form_id);
        $feeds = $this->map_feeds->get_feeds($form_id);

        foreach ($feeds as $feed) {
            if ($feed['id'] === $feed_id) {
                $current_feed = $feed;
                break;
            }
        }

        if (!isset($current_feed)) {
            wp_send_json( array('success' => 'false', 'errors' => "Sorry, we couldn't find the right feed.") );
        }

        $paging = array('offset' => $offset, 'page_size' => 100 );
        $processed = 0;
        $continue = true;

        // search for all entries that are active and do not have lat/lng coordinates
        $entry_search_criteria = array(
            "status" => "active",
            "field_filters" => array(
                array(
                    'key' => "gravity_forms_google_maps_coordinates",
                    'operator' => 'is',
                    'value' => ""
                )
            )
        );

        $entries = GFAPI::get_entries($form_id, $entry_search_criteria, array(), $paging);

        $map_settings = $this->form_map_settings->get_settings( $form );

        if (!is_wp_error($entries) && !empty($entries)) {
            foreach ($entries as $entry) {
                $addresses[$entry['id']] = $this->parse_address($entry, $map_settings);
            }
        } else {
            $continue = false;
        }

        $addresses_json = json_encode($addresses);

        $geocodio_api_key = get_option('gf_gm_geocodio_key');

        $geocodio_response = wp_remote_post(
            'https://api.geocod.io/v1/geocode?api_key=' . $geocodio_api_key,
            array(
                'headers' => array(
                    'Content-Type' => 'application/json'
                ),
                'body' => $addresses_json
            )
        );

        if (!is_wp_error( $geocodio_response )) {
            $coordinates = json_decode($geocodio_response['body'], true);
            $entries_updated = $this->update_coordinates($coordinates, $form['id'], $current_feed);
        } else {
            wp_send_json( array(
                'success' => 'false',
                'numprocessed' => 0,
                'continue_processing' => false,
                'errors' => $geocodio_response->get_error_message()
            ));
        }

        if (count($entries) < 100) {
            $continue = false;
        }

        wp_send_json( array(
            'success' => 'true',
            'numprocessed' => $entries_updated,
            'continue_processing' => $continue,
        ));

    }

    public function ajax_rebuild_pins() {

        $form_id = $_POST['form_id'];
        $feed_id = $_POST['feed_id'];
        $offset = (int)$_POST['offset'];

        $form = GFAPI::get_form($form_id);
        $feeds = $this->map_feeds->get_feeds($form_id);

        foreach ($feeds as $feed) {
            if ($feed['id'] === $feed_id) {
                $current_feed = $feed;
                break;
            }
        }

        if (!isset($current_feed)) {
            wp_send_json( array('success' => 'false', 'errors' => "Sorry, we couldn't find the right feed.") );
        }

        $paging = array('offset' => $offset, 'page_size' => 50 );
        $processed = 0;
        $continue = true;
        $numskipped = 0;

        $entries = GFAPI::get_entries($form_id, array(), array(), $paging);

        if (!is_wp_error($entries) && !empty($entries)) {
            foreach ($entries as $entry) {
                if ($this->map_feeds->is_feed_condition_met( $current_feed, $form, $entry )) {
                    $this->map_feeds->reprocess_feed($current_feed, $entry, $form);
                    $processed++;
                } else {
                    $numskipped++;
                }
            }
        } else {
            $continue = false;
        }

        if (count($entries) < 50) {
            $continue = false;
        }

        wp_send_json( array(
            'success' => 'true',
            'numprocessed' => $processed,
            'continue_processing' => $continue,
            'numskipped' => $numskipped
        ));
    }

    public function ajax_get_pins( $request ) {
        global $wpdb;
        $form_id = (int)$request->get_param('formid');
        $offset = (int)$_GET['offset'];
        $limit = (int)$_GET['limit'];
        $total_count = 0;
        $entry_ids = '';
        $final_entries = array();

        $form = GFAPI::get_form($form_id);

        if ( !$form) {
            wp_send_json_error( 'could not find form' );
        }

        $map_settings = $this->form_map_settings->get_settings( $form );

        // search for all entries that are active and have lat/lng coordinates
        $entry_search_criteria = array(
            "status" => "active",
            "field_filters" => array(
                array(
                    'key' => "gravity_forms_google_maps_coordinates",
                    'operator' => 'contains',
                    'value' => ","
                )
            )
        );

        // Only included starred entries if this map requires entry approval
        if ($map_settings["require_approval_checkbox"] === "1") {
            $entry_search_criteria['field_filters'][] = array(
                'key' => "is_starred",
                'operator' => 'is',
                'value' => "1"
            );
        }

        $entry_sorting = array(
            "key" => "id",
            "direction" => "ASC"
        );

        $entry_paging = array(
            'offset' => $offset,
            'page_size' => $limit
        );

        $entries = GFAPI::get_entries(
            $form_id,
            $entry_search_criteria,
            $entry_sorting,
            $entry_paging,
            $total_count
        );

        if (is_wp_error( $entries )) {
            wp_send_json_error($entries->get_error_message());
        } else if (empty($entries)) {
            wp_send_json_error('no entries found');
        }

        // find the first and last id from the entries, to limit the number of rows to grab from the database
        $first_entry = (int)$entries[0]['id'];
        end($entries);
        $last_key = key($entries);
        $last_entry = (int)$entries[$last_key]['id'];
        reset($entries);

        // Grab the entry_id and coordinates for every entry from this form in the range set above
        $lat_lng_query = $wpdb->prepare(
            "SELECT t1.entry_id, t1.meta_value AS 'coordinates', t2.meta_value AS 'infowindow', t3.meta_value AS 'pin'
            FROM  {$wpdb->prefix}gf_entry_meta t1
            INNER JOIN {$wpdb->prefix}gf_entry_meta t2
            ON t1.entry_id = t2.entry_id
            INNER JOIN {$wpdb->prefix}gf_entry_meta t3
            ON t1.entry_id = t3.entry_id
            WHERE t1.meta_key = 'gravity_forms_google_maps_coordinates'
            AND t1.meta_value <> ''
            AND t2.meta_key =  'gravity_forms_google_maps_infowindow'
            AND t3.meta_key =  'gravity_forms_google_maps_pin'
            AND t1.form_id = %d
            AND t1.entry_id >= %d
            AND t1.entry_id <= %d",
            $form_id,
            $first_entry,
            $last_entry
        );

        $cache_key = "gfgm" . $form_id . "_" . md5($lat_lng_query);

        $lat_lng_values = $this->get_cached_entries($cache_key, $lat_lng_query);

        if (empty($lat_lng_values)) {
            wp_send_json_error('no pins with latitude or longitude found');
        }

        // Using the entries and the meta query, populate an array to send back to the map
        foreach ($entries as $entry_key => $entry_value) {
            // skip if there is not a latitude or longitude set for this entry
            if (!array_key_exists($entry_value['id'], $lat_lng_values)) {
                continue;
            }

            // add this entry to the array of final entries
            $final_entries[(int)$entry_key] = array();

            // set the latitude and longitude
            $coordinates = explode(',', $lat_lng_values[$entry_value['id']]->coordinates);
            $final_entries[$entry_key]['latitude'] = $coordinates[0];
            $final_entries[$entry_key]['longitude'] = $coordinates[1];

            // set the pin
            $map_pin = $lat_lng_values[$entry_value['id']]->pin;
            if (empty($map_pin)) {
                $final_entries[$entry_key]['icon'] = "default";
            } else {
                $final_entries[$entry_key]['icon'] = $map_pin;
            }

            // set the infowindow content
            $final_entries[$entry_key]['infowindow'] = $lat_lng_values[$entry_value['id']]->infowindow;

        }

        if (!empty($final_entries)) {
            wp_send_json_success(array_values($final_entries));
        }

        wp_send_json_error( 'no pins found' );
    }

    protected function parse_address($entry, $field_map) {
        $address = "";
        $street = "";
        $city = "";
        $state = "";
        $country = "";
        $zip = "";
        $address_components = array("street","city","state","zip");

        unset($field_map['map_center']);
        unset($field_map['require_approval_checkbox']);
        unset($field_map['map_zoom_level']);
        unset($field_map['additional_map_options']);

        for ($i = 0; $i < count($address_components); $i++) {
            $key = $field_map['map_location_fields_' . $address_components[$i]];
            if (!empty($key)) {
                $val = array_key_exists($key, $entry) ? $entry[$key] : null;
                if (!is_null($val) && !empty($val) && $address_components[$i] !== "city") {
                    $address .= $val . ", ";
                } else if ($address_components[$i] === "city") {
                    $address .= $val . " ";
                }
            }
            unset($key,$val);
        }

        $address = trim($address,", ");

        return $address;
    }

    protected function update_coordinates($coordinates, $form_id, $feed) {
        global $wpdb;
        $count = 0;

        if (!array_key_exists('results', $coordinates)) {
            return 0;
        }

        foreach ($coordinates['results'] as $entry_id => $coordinate) {
            if ($coordinate['response']['results'][0]['accuracy'] > 0.5) {

                $latitude = (float)$coordinate['response']['results'][0]['location']['lat'] + (( rand(1,19) - 10 ) / 1000);
                $longitude = (float)$coordinate['response']['results'][0]['location']['lng'] + (( rand(1,19) - 10 ) / 1000);

                if ($feed['meta']['prevent_overlap_checkbox'] === "1") {
                    // add random value between -0.009 and 0.009
                    $latitude += (( rand(1,19) - 10 ) / 1000);
                    $longitude += (( rand(1,19) - 10 ) / 1000);
                }

                $response = $wpdb->update(
                    $wpdb->prefix . "gf_entry_meta",
                    array(
                        'form_id' => $form_id,
                        'entry_id' => $entry_id,
                        'meta_key' => 'gravity_forms_google_maps_coordinates',
                        'meta_value' => $latitude . ',' . $longitude
                    ),
                    array(
                        'form_id' => $form_id,
                        'entry_id' => $entry_id,
                        'meta_key' => 'gravity_forms_google_maps_coordinates',
                    ),
                    array(
                        '%d',
                        '%d',
                        '%s',
                        '%s'
                    ),
                    array(
                        '%d',
                        '%d',
                        '%s',
                    )
                );

                if ($response !== false) {
                    $count += $response;
                }
            }

            unset($row, $response);
        }

        return $count;
    }

    protected function get_cached_entries($cache_key, $query) {
        global $wpdb;

        $entries = get_transient($cache_key);

        if ($entries === false) {
            $entries = $wpdb->get_results($query, OBJECT_K);
            if ($entries) {
                set_transient( $cache_key, $entries, HOUR_IN_SECONDS );
            }
        }

        return $entries;
    }

    /**
     * Return an instance of this class.
     *
     * @since     0.1.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance( $form_map_settings, $map_feeds ) {

        if ( null == self::$instance ) {
            self::$instance = new self( $form_map_settings, $map_feeds );
        }

        return self::$instance;
    }

}
