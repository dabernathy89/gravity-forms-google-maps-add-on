<?php
/**
 * Gravity Forms Google Maps
 *
 * @package   Gravity_Forms_Google_Maps
 * @author    Daniel Abernathy <dabernathy89@gmail.com>
 * @license   GPL-2.0+
 * @copyright 2013 Daniel Abernathy
 */

class Gravity_Forms_Google_Maps_Form_Settings extends GFAddOn {

	protected static $instance = null;
    protected $_version = "2.0.0";
    protected $_min_gravityforms_version = "1.8.5";
    protected $_slug = "gf-google-maps-form-settings";
    protected $_path;
    protected $_full_path = __FILE__;
    protected $_title = "Gravity Forms Google Maps Add-On Form Settings";
    protected $_short_title = "Google Maps Settings";

    public function __construct() {
        $this->_path = plugin_basename( __FILE__ );
        parent::__construct();
    }

    public function get_settings( $form ) {
        return $this->get_form_settings($form);
    }

    public function pre_init() {
        parent::pre_init();

        $this->prepare_json_textarea_data();

        // Allow our JavaScript to access the form settings
        add_filter("gform_form_tag", array($this, "enqueue_map_settings_js"), 10, 2);

        // EDD Auto Updating and Licensing
        add_action('admin_init', array($this, 'gf_gm_register_option'));
        add_action('admin_init', array($this, 'gf_gm_activate_license'));
        add_action('admin_init', array($this, 'gf_gm_deactivate_license'));
    }

    protected function prepare_json_textarea_data() {
        if (array_key_exists('_gaddon_setting_additional_map_options', $_POST))
        {
            $json = $_POST['_gaddon_setting_additional_map_options'];
            $json = trim($json,"'");
            $json = ltrim($json,"'");

            // check if it's valid json
            $decoded_json = json_decode(stripslashes_deep($json));

            if (json_last_error() !== JSON_ERROR_NONE) {
                $json = "";
            } else {
                $json = "'" . $json . "'";
            }

            $_POST['_gaddon_setting_additional_map_options'] = $json;

        }
    }

    /*
    /*  Send add-on settings to javascript
    */
    public function enqueue_map_settings_js($form_tag, $form) {
        $settings = $this->get_form_settings($form);
        if (!empty($settings)) {
            unset($settings['map_center']);
            unset($settings['require_approval_checkbox']);
            unset($settings['map_zoom_level']);
            unset($settings['additional_map_options']);

            $script = "<script type='text/javascript'>";
            $script .= "window['gravity_forms_google_maps_" . $form['id'] . "'] = " . json_encode($settings) . ";";
            $script .= "</script>";
            $form_tag .= $script;
        }
        return $form_tag;
    }

    public function form_settings_fields($form) {

        $form_id = $form['id'];

        $zoom_level_choices = array();
        for ($i=0; $i < 20; $i++) {
            $zoom_level_choices[$i] = array(
                "label" => (string)$i,
                "name"  => "zoom_level_" . $i
            );
        }

        return array(
            array(
                "title"  => "Google Maps Settings",
                "fields" => array(
                    array(
                        "label"   => "Require entry approval",
                        "type"    => "checkbox",
                        "name"    => "require_approval",
                        "choices" => array(
                            array(
                                "label" => "Yes, hide entries until they are starred",
                                "name"  => "require_approval_checkbox"
                            )
                        )
                    ),
                    array(
                        "label"   => "Map Center (lat/lng)",
                        "type"    => "text",
                        "name"    => "map_center",
                        "tooltip" => "Example: <code>32.6957,-97.6024</code><br />If left blank, the geographic center of the United States will be used."
                    ),
                    array(
                        "label"   => "Additional Map Options",
                        "type"    => "json_textarea",
                        "name"    => "additional_map_options",
                        "tooltip" => "Must be valid JSON. <a target='_blank' href='https://developers.google.com/maps/documentation/javascript/reference#MapOptions'>See documentation here.</a>"
                    ),
                    array(
                        "label"   => "Map Zoom Level",
                        "type"    => "select",
                        "name"    => "map_zoom_level",
                        "choices" => $zoom_level_choices
                    ),
                    array(
                        "label"   => "Map Location Fields",
                        "type"    => "field_map",
                        "name"    => "map_location_fields",
                        "tooltip" => "In order for the entry to be geocoded, at least one of these should be included in the form.",
                        "field_map" => array(
                            array(
                                "name" => "street",
                                "label" => "Street"
                            ),
                            array(
                                "name" => "city",
                                "label" => "City"
                            ),
                            array(
                                "name" => "state",
                                "label" => "State"
                            ),
                            array(
                                "name" => "zip",
                                "label" => "Zip"
                            ),
                            array(
                                "name" => "country",
                                "label" => "Country"
                            )
                        )
                    ),
                )
            )
        );
    }

    public function settings_json_textarea($field, $echo = true) {
        $value = $this->get_setting($field["name"], null);
        $value = ltrim($value, "'");
        $value = trim($value, "'");

        $attributes = $this->get_field_attributes($field);
        $default_value = rgar( $field, 'value' ) ? rgar( $field, 'value' ) : rgar( $field, 'default_value' );

        $name = '' . esc_attr($field["name"]);
        $tooltip =  isset( $choice['tooltip'] ) ? gform_tooltip( $choice['tooltip'], rgar( $choice, 'tooltip_class'), true ) : "";
        $html = "";

        $html .= '<textarea
                    name="_gaddon_setting_' . $name . '" ' .
                    implode( ' ', $attributes ) .
                 '>' .
                    esc_html($value) .
                '</textarea>';

        if( $this->field_failed_validation( $field ) )
            $html .= $this->get_error_icon( $field );

        if ($echo)
            echo $html;

        return $html;
    }

    public function plugin_settings() {
        $google_api_key    = get_option( 'gf_gm_google_api_key' );
        $license    = get_option( 'gf_gm_license_key' );
        $status     = get_option( 'gf_gm_license_status' );
        $geocodio_key = get_option( 'gf_gm_geocodio_key' );

        ?>
        <div class="wrap">
            <form method="post" action="options.php">

                <?php settings_fields('gf_gm_license'); ?>

                <table class="form-table">
                    <tbody>
                        <tr valign="top">
                            <th scope="row" valign="top">Google API Key</th>
                            <td>
                                <input id="gf_gm_google_api_key" name="gf_gm_google_api_key" type="text" class="regular-text" value="<?php echo esc_attr( $google_api_key ); ?>" />
                                <label class="description" for="gf_gm_google_api_key">Enter your API key</label>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row" valign="top">License Key</th>
                            <td>
                                <input id="gf_gm_license_key" name="gf_gm_license_key" type="text" class="regular-text" value="<?php echo esc_attr( $license ); ?>" />
                                <label class="description" for="gf_gm_license_key">Enter your license key</label>
                            </td>
                        </tr>
                        <?php if( false !== $license ) { ?>
                            <tr valign="top">
                                <th scope="row" valign="top">Activate License</th>
                                <td>
                                    <?php if( $status !== false && $status == 'valid' ) { ?>
                                        <?php wp_nonce_field( 'gf_gm_nonce', 'gf_gm_nonce' ); ?>
                                        <input type="submit" class="button-secondary" name="gf_gm_license_deactivate" value="Deactivate License"/>
                                        <br />
                                        <span style="color:green;">active</span>
                                    <?php } else {
                                        wp_nonce_field( 'gf_gm_nonce', 'gf_gm_nonce' ); ?>
                                        <input type="submit" class="button-secondary" name="gf_gm_license_activate" value="Activate License"/>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>

                        <tr valign="top">
                            <th scope="row" valign="top">Geocodio API Key</th>
                            <td>
                                <input id="gf_gm_geocodio_key" name="gf_gm_geocodio_key" type="text" class="regular-text" value="<?php echo esc_attr( $geocodio_key ); ?>" />
                                <label class="description" for="gf_gm_geocodio_key">Enter your license key</label>
                            </td>
                        </tr>

                    </tbody>
                </table>
                <?php submit_button(); ?>
            </form>
        <?php
    }

    public function gf_gm_sanitize_license( $new ) {
        $old = get_option( 'gf_gm_license_key' );
        if( $old && $old != $new ) {
            delete_option( 'gf_gm_license_status' );
        }
        return $new;
    }

    public function gf_gm_activate_license() {

        if( isset( $_POST['gf_gm_license_activate'] ) ) {

            if( ! check_admin_referer( 'gf_gm_nonce', 'gf_gm_nonce' ) ) {
                return;
            }

            $license = trim( get_option( 'gf_gm_license_key' ) );

            $api_params = array(
                'edd_action'=> 'activate_license',
                'license'   => $license,
                'item_name' => urlencode( GF_GM_ITEM_NAME ), // the name of our product in EDD
                'url'       => home_url()
            );

            $response = wp_remote_get( add_query_arg( $api_params, GF_GM_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

            if ( is_wp_error( $response ) ) {
                return false;
            }

            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

            update_option( 'gf_gm_license_status', $license_data->license );
        }
    }

    public function gf_gm_deactivate_license() {

        if( isset( $_POST['gf_gm_license_deactivate'] ) ) {

            if( ! check_admin_referer( 'gf_gm_nonce', 'gf_gm_nonce' ) ) {
                return;
            }

            $license = trim( get_option( 'gf_gm_license_key' ) );

            $api_params = array(
                'edd_action'=> 'deactivate_license',
                'license'   => $license,
                'item_name' => urlencode( GF_GM_ITEM_NAME ), // the name of our product in EDD
                'url'       => home_url()
            );

            $response = wp_remote_get( add_query_arg( $api_params, GF_GM_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );

            if ( is_wp_error( $response ) ) {
                return false;
            }

            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

            // $license_data->license will be either "deactivated" or "failed"
            if( $license_data->license == 'deactivated' ) {
                delete_option( 'gf_gm_license_status' );
            }
        }
    }

    public function gf_gm_register_option() {
        register_setting('gf_gm_license', 'gf_gm_license_key', array($this, 'gf_gm_sanitize_license') );
        register_setting('gf_gm_license', 'gf_gm_geocodio_key' );
        register_setting('gf_gm_license', 'gf_gm_google_api_key' );
    }

    public function plugin_settings_title(){
        return "Google Maps Add-On";
    }

	/**
	 * Return the plugin slug.
	 *
	 * @since    0.1.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.1.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

}
