jQuery( document ).ready(function( $ ) {

    gf_google_maps_initialize();

    $('.gfgooglemap_wrapper').each(function(index, el) {
        var current_form_wrap = $(this);
        var form_id = current_form_wrap.find('form.gfgooglemap').attr('id');
        var input_id;
        var current_form;
        form_id = form_id.replace('gform_','');

        // This is a Google Maps form if this variable is set.
        if ( (typeof window['gravity_forms_google_maps_' + form_id]) !== "undefined" ) {
            var field_map = window['gravity_forms_google_maps_' + form_id];
            current_form_wrap.on('submit', 'form', function(event) {
                current_form = current_form_wrap.children('form.gfgooglemap');

                if (current_form.hasClass('submitting')) {
                    return;
                }

                // Not on the last page of the form
                if (current_form.find('.gf_step_active').length && !current_form.find('.gf_step_active').is('.gf_step_last')) {
                    return;
                }

                event.preventDefault();
                current_form.addClass('submitting');

                var geolocate_string = "", street = "", city = "", state = "", country = "", zip = "";
                address_components = ["street","city","state","zip","country"];

                for (var i = 0; i < address_components.length; i++) {
                    if (field_map['map_location_fields_' + address_components[i]] !== "" ) {
                        input_id = field_map['map_location_fields_' + address_components[i]];
                        input_id = input_id.replace(/\./g,'_');
                        var field = current_form.find('#input_' + form_id + '_' + input_id);
                        if (field.length > 0 && field.val() !== "") {
                            geolocate_string += field.val() + ", ";
                        }
                    }
                }

                geolocate_string = geolocate_string.trim();
                geolocate_string = geolocate_string.replace(/,$/,'');
                geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': geolocate_string }, function(results, status) {
                    if (status === 'OK') {
                        // insert new form fields for latitude and longitude
                        var latitude = results[0]['geometry']['location'].lat();
                        var longitude = results[0]['geometry']['location'].lng();
                        current_form.append('<input type="hidden" name="gravity_forms_google_maps_' + form_id + '_lat" value="' + latitude + '" />');
                        current_form.append('<input type="hidden" name="gravity_forms_google_maps_' + form_id + '_lng" value="' + longitude + '" />');
                    }
                    current_form.submit();
                });
            });
        }
    });

    function gf_google_maps_initialize() {
        var map_id;
        var map_settings;
        var map_options;
        var pins_offset = 0;

        $('.gravity-form-google-map').each(function(index, el) {
            map_id = $(this).attr('id');
            map_id = map_id.replace('gravity-form-google-map-','');
            // Find the settings for this map, do nothing if they are not set
            if ( (typeof window['gravity_forms_google_maps_settings_' + map_id]) !== "undefined" ) {
                map_settings = window['gravity_forms_google_maps_settings_' + map_id];
                map_options = {
                    center: new google.maps.LatLng(map_settings['lat'], map_settings['lng']),
                    zoom: parseInt(map_settings['zoom'],10),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    streetViewControl: false,
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE
                    }
                };

                if ( (typeof map_settings['additional_map_options']) !== "undefined" ) {
                    for (var map_option in map_settings['additional_map_options']) {
                        map_options[map_option] = map_settings['additional_map_options'][map_option];
                    }
                }

                // Initiate the map, and make it globally accessible
                window['gf_map_' + map_settings['form_id']] = new google.maps.Map(document.getElementById('gravity-form-google-map-' + map_settings['form_id']), map_options);

                get_pins(0, map_id, map_settings);
            }
        });
    }

    function get_pins(offset, map_id, map_settings) {
        $.ajax({
            type: 'GET',
            url: map_settings['ajaxurl'],
            data: {
                "offset" : offset,
                "limit" : 200
            },
            complete: function(response){
                if (response['responseJSON'].success && typeof(response['responseJSON']) != "undefined" && response['responseJSON'].data.length > 0) {
                    gf_google_maps_add_pins_to_map(response['responseJSON'].data, map_id);
                    get_pins(offset + 200, map_id, map_settings);
                }
            }
        });
    }

    function gf_google_maps_add_pins_to_map(pins, map_id) {
        var marker;
        var map = window['gf_map_' + map_id];
        if (!map.info_window) {
            map.info_window = new google.maps.InfoWindow();
        }
        var info_window_html;

        for (var i = 0; i < pins.length; i++) {
            if (pins[i]['icon'] !== "default") {
                marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(pins[i]['latitude'], pins[i]['longitude']),
                    icon: pins[i]['icon']
                });
            } else {
                marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(pins[i]['latitude'], pins[i]['longitude'])
                });
            }
            info_window_html = pins[i]['infowindow'];
            if (info_window_html) {
                bindInfoWindow(marker, map, map.info_window, info_window_html);
            }
        }
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
        google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        });
    }

});