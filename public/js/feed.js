jQuery(document).ready(function($) {

    if ($('#infowindow_text').length) {
        window.gfMergeTagsObj(window.form, $('#infowindow_text'));
    }

    $('.geocode-empty-pins').on('click', function(event) {
        event.preventDefault();
        var button = $(this);

        $('p.geocode-empty-results, p.geocode-empty-errors, .spinner').remove();
        button.attr("disabled", "disabled").after('<p class="geocode-empty-results">Geocoded <span class="count">0</span> entries</p><p class="geocode-empty-errors"></p>');

        geocode_empty_pins(0, button);
    });

    function geocode_empty_pins(offset, button) {
        jQuery.ajax(
            ajaxurl,
            {
                type: 'POST',
                data: {
                    "action": "gravity_forms_google_map_geocode_empty_pins",
                    "form_id": button.attr('data-current-form'),
                    "feed_id": button.attr('data-current-feed'),
                    "offset": offset
                }
            }
        )
        .done(function( data ) {
            offset += 100;
            var numprocessed = parseInt($('p.geocode-empty-results .count').html(),10);
            if (data.success === "true") {
                $('p.geocode-empty-results .count').html(numprocessed + parseInt(data.numprocessed,10));
            } else {
                $('p.geocode-empty--errors').html(data.errors);
            }

            if (data.continue_processing) {
                geocode_empty_pins(offset, button);
            } else {
                button.removeAttr('disabled');
                $('p.geocode-empty-results').append('<br />Done!');
            }

        })
        .fail(function( data ) {
            console.log(data);
            button.removeAttr('disabled');
            $('p.geocode-empty--errors').html('failed');
        });
    }

    $('.rebuild-pins').on('click', function(event) {
        event.preventDefault();
        var button = $(this);

        $('p.rebuild-results, p.rebuild-errors, .spinner').remove();
        button.attr("disabled", "disabled").after('<p class="rebuild-results">Rebuilt <span class="count">0</span> pins</p><p class="rebuild-errors"></p>');

        rebuild_pins(0, button);
    });

    function rebuild_pins(offset, button) {
        jQuery.ajax(
            ajaxurl,
            {
                type: 'POST',
                data: {
                    "action": "gravity_forms_google_map_rebuild_pins",
                    "form_id": button.attr('data-current-form'),
                    "feed_id": button.attr('data-current-feed'),
                    "offset": offset
                }
            }
        )
        .done(function( data ) {
            offset += 50;
            var numprocessed = parseInt($('p.rebuild-results .count').html(),10);
            if (data.success === "true") {
                $('p.rebuild-results .count').html(numprocessed + parseInt(data.numprocessed,10));
            } else {
                $('p.rebuild-errors').html(data.errors);
            }

            if (data.continue_processing) {
                rebuild_pins(offset, button);
            } else {
                button.removeAttr('disabled');
                $('p.rebuild-results').append('<br />Done!');
            }

        })
        .fail(function( data ) {
            console.log(data);
            button.removeAttr('disabled');
            $('p.rebuild-errors').html('failed');
        });
    }

});