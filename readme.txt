=== Gravity Forms Google Maps Add-On ===
Contributors: dabernathy89
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gravity Forms Google Maps Add-On allows any website to populate a Google Map with geolocated form entries.

== Description ==

The Gravity Forms Google Maps Add-On allows any website to populate a Google Map with geolocated form entries. The add-on introduces two new menu screens to each form’s settings page: Google Maps Settings and Google Maps Feeds. The add-on also adds a shortcode that can be used to display the map for a form on any page or post.

_Google Maps Settings_

The Google Maps Settings page displays settings for each form's Google Map shortcode and determines which fields will be used for geolocation. The settings on this page are:

1. Require Entry Approval
  1. If checked, entries on this form will only be displayed on the map if they are "starred"

2. Map Center
  1. This defines the default map center in Decimal Degrees

3. Map Zoom Level
  1. This defines the default map zoom level; a higher number corresponds to a closer zoom

4. Map Location Fields
  1. At least one form field must be mapped to one of the following address types:
    1. Street
    2. City
    3. State
    4. Zip
    5. Country

_Google Maps Feeds_

At least one feed must be set up for entries to be geocoded and stored as map icons. Multiple feeds may be set up if you wish to change the way entries are displayed based on the user's input. The feed settings are:

1. Feed Name
  1. An identifier for this feed (only shown in the admin area)

2. Icon
  1. A custom map pin for this feed - should be a full URL

3. Google Maps InfoWindow Text
  1. A template for the content that will be assigned to that entry's info window. The template can include HTML, and implements Gravity Forms' merge tag functionality to include entry-specific content.

4. Prevent icon overlap
  1. Some forms may need to shift icons to prevent overlap. For example, if your form's most specific address field is "City", form entries from the same city will overlap each other. Check this box to add a small random adjustment to the pins.

5. Make this feed conditional
  1. Instructs the plugin to only apply these settings if certain conditions from the user's input are met. Note: you should make your feeds mutually exclusive so that each entry is only processed once. This means that a form should not have a non-conditional feed at the same time as conditional feeds.

_Displaying a form's Google Map_

To display the Google Map associated with a form, add the following shortcode to any post or page, replacing the number 1 with the form's ID:

[gfgooglemap formid="1"]

== Changelog ==

= 2.0.0 =
* Change to REST API for public facing AJAX endpoint; implement some internal caching

= 1.2.0 =
* Add setting for Google API Key

= 1.1.3 =
* Bug fix - 1.9 compatibility

= 1.1.2 =
* Bug fix - fix for re-geocoding

= 1.1.1 =
* Bug fix - remove notices from being thrown while geocoding pins from feed settings page

= 1.1.0 =
* Added - you can now geocode entries that do not have lat/lng info (either because they existed before setting up the plugin, or because their address could not be geocoded when they submitted the form). requires an API key from geocod.io

= 1.0.8 =
* Bug fix - fix for geocoding on AJAX multistep forms

= 1.0.7 =
* Bug fix - fix for PHP notice when lat/lng is missing

= 1.0.6 =
* Bug fix - complex field types were not being geocoded properly

= 1.0.5 =
* Added - you can now provide custom map options under form settings

= 1.0.4 =
* Bug fix - map now works even if some entries are removed for not having latitude or longitude set

= 1.0.2 =
* Bug fix - default lat/lng now working
* Added - merge tag functionality now supported for infowindow boxes

= 1.0.1 =
* Bug fix - infowindow should no longer appears if content is empty

= 1.0 =
* First stable version.